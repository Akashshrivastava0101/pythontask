Python 3.7.9 (tags/v3.7.9:13c94747c7, Aug 17 2020, 16:30:00) [MSC v.1900 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
>>> # input values to list
arr = [12, 3, 4, 15]
  
# sum() is an inbuilt function in python that adds 
# all the elements in list,set and tuples and returns
# the value 
ans = sum(arr)
  
# display sum
print ('Sum of the array is ',ans)
  